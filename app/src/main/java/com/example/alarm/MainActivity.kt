package com.example.alarm;

import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DateFormat
import java.util.*

class MainActivity : AppCompatActivity(), TimePickerDialog.OnTimeSetListener{
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)
        calendar.set(Calendar.SECOND, 0)

        updateTimeText(calendar)
        startAlarm(calendar)
    }

    private fun updateTimeText(calendar: Calendar){
        val timeText = DateFormat.getTimeInstance(DateFormat.SHORT).format(calendar.time)
        val description = edt_description.text.toString()
        text_view.text = timeText
        text_description.text = description
    }

    private fun startAlarm(calendar: Calendar){
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, AlertReceiver::class.java)
        intent.putExtra("description", edt_description.text.toString())
        val pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);

        if(calendar.before(Calendar.getInstance())) {
            calendar.add(Calendar.DATE, 1)
        }

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_cancel_alarm.setOnClickListener {
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Information")
            builder.setMessage("Are u sure ?")
            builder.setPositiveButton("aye") {dialog, which ->
                cancelAlarm()
            }
            builder.setNegativeButton("nah") {dialog, which ->
                dialog.dismiss()
            }
            builder.create().show()
        }

        btn_set_timer.setOnClickListener {

            if(edt_description.text.isNullOrEmpty()){
                edt_layout_description.error = "The description cannot be empty"
                return@setOnClickListener
            }
            edt_layout_description.isErrorEnabled = false
            val timePicker: DialogFragment = TimePickerFragment()
            timePicker.show(supportFragmentManager, "Time Picker")

        }


    }

    private fun cancelAlarm() {
        val alarmManager: AlarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this,AlertReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 1,  intent, 0)

        alarmManager.cancel(pendingIntent)
        text_view.text = "Alarm Canceled"
    }

}